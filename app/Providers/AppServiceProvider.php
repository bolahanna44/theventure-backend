<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SendgridWrapper;
use App\Services\ChuckNorris;
use App\Contracts\MailSenderContract;
use App\Contracts\JokeServiceContract;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(MailSenderContract::class, SendgridWrapper::class);
        $this->app->bind(JokeServiceContract::class, ChuckNorris::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
