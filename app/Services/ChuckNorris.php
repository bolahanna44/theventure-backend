<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use App\Contracts\JokeServiceContract;

class ChuckNorris implements JokeServiceContract
{
    const BASE_URL = 'https://api.chucknorris.io';

    public function getJoke()
    {
        $response = Http::get(self::BASE_URL . '/jokes/random');

        if ($response->status() === 200) {
            return $response->json()["value"];
        } else {
            throw new Exception('Chuck Norris API is not available');
        }
    }
}
