<?php

namespace App\Services;

use SendGrid\Mail\Mail;
use App\Contracts\MailSenderContract;

class SendgridWrapper implements MailSenderContract
{
    public function sendEmails($tos, $subject, $content)
    {
        $email = new Mail();
        $email->setFrom("bola.hanna55@gmail.com", "Bola Hanna");
        $email->addTos(array_combine($tos, $tos));
        $email->setSubject($subject);
        $email->addContent("text/plain", $content);
        $sendgrid = new \SendGrid($_ENV['SENDGRID_API_KEY']);
        $response = $sendgrid->send($email);
        return $response->body();
    }
}
