<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Contracts\MailSenderContract;
use App\Contracts\JokeServiceContract;
use Illuminate\Support\Facades\Log;


class SendJokeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emails;
    public $tries = 3;
    /**
     * Create a new job instance.
     */
    public function __construct($emails)
    {
        $this->emails = $emails;
    }

    /**
     * Execute the job.
     */
    public function handle(MailSenderContract $mailSender, JokeServiceContract $jokeService): void
    {
        Log::info('Sending joke email');
        $joke = $jokeService->getJoke();
        $mailSender->sendEmails($this->emails, 'ChuckNorris Joke', $joke);
    }
}
