<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Jobs\ProcessEmailsCsv;
use Illuminate\Http\Response;

class EmailFilesController extends Controller
{
    public function create(Request $request)
    {
        // Validate uploaded file
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,txt|max:100000',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        // Store uploaded file
        $uuid = uuid_create();
        $path = $request->file('file')->storeAs('email_files', $uuid);
        // Dispatch job to send emails
        ProcessEmailsCsv::dispatchSync($path);
        return response($path, Response::HTTP_ACCEPTED);
    }
}
