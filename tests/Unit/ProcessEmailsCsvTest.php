<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;
use App\Jobs\ProcessEmailsCsv;
use App\Jobs\SendJokeEmail;

class ProcessEmailsCsvTest extends TestCase
{
    public function testHandle()
    {
        Queue::fake();
        $csvData = [
            ['Emails'],
            ['john.doe@example.com'],
            ['invalid-email'], // Invalid email
            ['jane.smith@example.com'],
        ];

        // Create a temporary CSV file
        $csvPath = $this->createTempCsvFile($csvData);



        ProcessEmailsCsv::dispatchSync($csvPath);
        $validEmails = [];
        // Set up an expectation that dispatchSync will be called with $lines as argument
        Queue::assertPushed(SendJokeEmail::class);
        // Dispatch the ProcessEmailsCsv job


    }

    // Helper method to create a temporary CSV file with provided data
    private function createTempCsvFile($data)
    {
        $csvPath = storage_path('app/temp_emails.csv');
        $file = fopen($csvPath, 'w');

        foreach ($data as $row) {
            fputcsv($file, $row);
        }

        fclose($file);
        return 'temp_emails.csv';
    }
}
