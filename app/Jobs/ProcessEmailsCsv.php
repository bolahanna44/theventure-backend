<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\Jobs\SendJokeEmail;
use \Exception;

class ProcessEmailsCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 0;

    const BATCH_SIZE = 900;
    const MAX_EMAILS_COUNT = 10000;

    protected $path;
    /**
     * Create a new job instance.
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $filePath = storage_path('app/' . $this->path);
        $this->validateEmailsAndRowsCount($filePath);
        $file = fopen($filePath, 'r');
        $lines = [];
        if ($file) {
            try {

                fgetcsv($file); // skip the header
                while (($line = fgetcsv($file)) !== false) { // iterate over each line in the csv
                    $lines[] = $line[0];
                    if (count($lines) === self::BATCH_SIZE) {
                        Log::info("processing batch of " . self::BATCH_SIZE . ' in file ' . $this->path);
                        SendJokeEmail::dispatchSync($lines);
                        $lines = [];
                    }
                }

                if (!empty($lines)) {
                    Log::info("processing batch of " . count($lines) . ' in file ' . $this->path);
                    Log::info($lines);
                    SendJokeEmail::dispatch($lines);
                }
            } catch (Throwable $e) {
                Log::error('Error reading the CSV file: ' . $e->getMessage());
            } finally {
                fclose($file);
            }
        } else {
            throw new Exception("file does not exist");
        }
    }

  private function validateEmailsAndRowsCount($filePath) {
      if (!file_exists($filePath)) {
          throw new Exception("File does not exist");
      }

      $file = fopen($filePath, 'r');
      $lineCount = 0;
      fgetcsv($file); // skip the header

      while (($line = fgetcsv($file)) !== false) {
          $lineCount++;

          if (!$this->isValidEmail($line[0])) {
              fclose($file);
              throw new Exception("File contains an invalid email: " . $line[0] . " in line " . $lineCount);
          }

          if ($lineCount > self::MAX_EMAILS_COUNT) {
              fclose($file);
              throw new Exception("File has too many rows");
          }
      }

      fclose($file);

      return true;
  }


    private function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
}
