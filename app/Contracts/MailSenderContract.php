<?php

namespace App\Contracts;

interface MailSenderContract
{
    public function sendEmails(array $to, string $subject, string $body);
}
