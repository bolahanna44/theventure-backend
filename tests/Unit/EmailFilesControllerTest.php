<?php

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Tests\TestCase;

class EmailFilesControllerTest extends TestCase
{
    // Your test method
    public function testCreate()
    {
        // Prepare a sample file to upload
        $file = UploadedFile::fake()->create('test.csv', 1000); // file size = 1000 bytes

        // Send a POST request to your controller action
        $response = $this->post('/api/v1/email_files', ['file' => $file]);

        // Assert that the response has the correct status code and content
        $response->assertStatus(Response::HTTP_ACCEPTED);

        // Assert that the job was dispatched with the correct file path
        $file_path = $response->getContent();

        Bus::spy();
        Bus::assertDispatchedSync(ProcessEmailsCsv::class, function ($job) use ($file_path) {
            return $job->path === $file_path;
        });

        // Assert that the file was stored correctly
        Storage::assertExists($file_path);
    }

    public function testCreateWithInvalidFile()
        {
            // Prepare an invalid file to upload (file type not allowed)
            $invalidFile = UploadedFile::fake()->create('test.pdf', 1000); // file size = 1000 bytes

            // Send a POST request to your controller action with the invalid file
            $response = $this->post('/api/v1/email_files', ['file' => $invalidFile]);

            // Assert that the response has the correct status code (400 Bad Request)
            $response->assertStatus(Response::HTTP_BAD_REQUEST);
        }
}
